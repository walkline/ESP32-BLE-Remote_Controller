"""
键盘 Report Map 定义，没有 Report ID
"""
REPORT_MAP_DATA = [
	0x05, 0x01, # USAGE_PAGE (Generic Desktop)
	0x09, 0x06, # USAGE (Keyboard)
	0xA1, 0x01, # COLLECTION (Application)
	# 8 keycode
	0x05, 0x07, # 	USAGE_PAGE (Keyboard)
	0x19, 0xE0, # 	USAGE_MINIMUM (Keyboard LeftControl)
	0x29, 0xE7, # 	USAGE_MAXIMUM (Keyboard Right GUI)
	# keycode value 0 and 1
	0x15, 0x00, # 	LOGICAL_MINIMUM (0)
	0x25, 0x01, # 	LOGICAL_MAXIMUM (1)
	# Modifier byte
	0x75, 0x01, # 	REPORT_SIZE (1)
	0x95, 0x08, # 	REPORT_COUNT (8)
	0x81, 0x02, # 	INPUT (Data,Var,Abs)
	# Reserved byte
	0x95, 0x01, # 	REPORT_COUNT (1)
	0x75, 0x08, # 	REPORT_SIZE (8)
	0x81, 0x01, # 	INPUT (Cnst,Var,Abs)
	# # LED report
	0x95, 0x05, # 	REPORT_COUNT (5)
	0x75, 0x01, # 	REPORT_SIZE (1)
	0x05, 0x08, # 	USAGE_PAGE (LEDs)
	0x19, 0x01, # 	USAGE_MINIMUM (Num Lock)
	0x29, 0x05, # 	USAGE_MAXIMUM (Kana)
	0x91, 0x02, # 	OUTPUT (Data,Var,Abs)
	# # LED report padding
	0x95, 0x01, # 	REPORT_COUNT (1)
	0x75, 0x03, # 	REPORT_SIZE (3)
	0x91, 0x03, # 	OUTPUT (Cnst,Var,Abs)
	# Key arrays (6 bytes)
	0x95, 0x06, # 	REPORT_COUNT (6)
	0x75, 0x08, # 	REPORT_SIZE (8)
	0x15, 0x00, # 	LOGICAL_MINIMUM (0)
	0x25, 0x81, # 	LOGICAL_MAXIMUM (129)
	0x05, 0x07, # 	USAGE_PAGE (Keyboard)
	0x19, 0x00, # 	USAGE_MINIMUM (Reserved (no event indicated))
	0x29, 0x81, # 	USAGE_MAXIMUM (Keyboard Application)
	0x81, 0x00, # 	INPUT (Data,Ary,Abs)
	0xC0,		# END_COLLECTION
]

"""
用于发送 Volume Down 键码
"""
def send_key_press(self):
	if self.__conn_handle is not None:
		self.__write(self.__handle_report, bytes([0x0, 0x0, 0x81, 0x0, 0x0, 0x0, 0x0, 0x0]))
		self.__notify(self.__conn_handle, self.__handle_report)
		self.__write(self.__handle_report, bytes([0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0]))
		self.__notify(self.__conn_handle, self.__handle_report)
