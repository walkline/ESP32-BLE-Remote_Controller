<h1 align="center">ESP32 BLE - Remote Controller</h1>

<p align="center"><img src="https://img.shields.io/badge/Licence-MIT-green.svg?style=for-the-badge" /></p>

### 项目介绍

[MicroPython for ESP32 开发板低功耗蓝牙（BLE）研究学习项目](https://gitee.com/walkline/esp32-ble)的分支项目，用`ESP32 开发板`的板载按钮做个自拍遥控器吧

### 自拍遥控器原理？

它就是一个按钮，通过蓝牙连接到你的手机，然后打开相机，按一下按钮，就实现了无线拍照功能，也就是自拍遥控器

> 前提是手机相机支持`音量键`拍照

> 经测试，只能控制安卓手机，苹果手机暂时无解

### 如何使用呢？

简单说分为如下步骤：

* 下载并烧录自定义的固件到开发板
* 如果你用的是官方最新固件的话可以参考`不想烧录自定义固件？`部分
* 把项目目录下的`ble`文件夹上传到开发板
* 在开发板上运行`ble\remote_controller.py`
* 打开手机蓝牙搜索并连接`RemoteCon`设备
* 连接成功后打开手机相机，按一下开发板上的`BOOT`按钮完成拍照

![Screenshot](images/screenshot.png)

### 下载烧录自定义固件

访问 [自定义固件下载项目](https://gitee.com/walkline/esp32_firmware) 下载最新的自定义固件，并参考 [附录1：如何刷写固件](https://gitee.com/walkline/esp32_firmware#%E9%99%84%E5%BD%951%E5%A6%82%E4%BD%95%E5%88%B7%E5%86%99%E5%9B%BA%E4%BB%B6) 烧录固件到开发板

### 不想烧录自定义固件？

当然没问题，不过要确认你现在的固件是**支持`ble`的**，然后

* 下载 [const.py](https://gitee.com/walkline/micropython-ble-library/raw/master/ble/const.py)

* 下载 [tools.py](https://gitee.com/walkline/micropython-ble-library/raw/master/ble/tools.py)

保存到项目目录`ble`文件夹下，一起上传到开发板即可

### 合作交流

* 联系邮箱：<walkline@163.com>
* QQ 交流群：
    * 走线物联：163271910
    * 扇贝物联：31324057

<p align="center"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_walkline.png" width="300px" alt="走线物联"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_bigiot.png" width="300px" alt="扇贝物联"></p>
