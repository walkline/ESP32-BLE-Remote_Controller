"""
The MIT License (MIT)
Copyright © 2020 Walkline Wang (https://walkline.wang)
Gitee: https://gitee.com/walkline/ESP32-BLE-Remote_Controller
"""
import ubluetooth as bt
from ble.const import BLEConst
from .__characteristic import Characteristic

class HIDControlPoint(Characteristic):
	UUID = bt.UUID(BLEConst.Characteristics.HID_CONTROL_POINT)
	FLAGS = bt.FLAG_WRITE

	def __init__(self):
		Characteristic.__init__(self, self.UUID, self.FLAGS)
