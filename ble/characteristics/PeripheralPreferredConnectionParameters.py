"""
The MIT License (MIT)
Copyright © 2020 Walkline Wang (https://walkline.wang)
Gitee: https://gitee.com/walkline/ESP32-BLE-Remote_Controller
"""
import ubluetooth as bt
from ble.const import BLEConst
from .__characteristic import Characteristic

class PeripheralPreferredConnectionParameters(Characteristic):
	UUID = bt.UUID(BLEConst.Characteristics.PERIPHERAL_PREFERRED_CONNECTION_PARAMETERS)
	FLAGS = bt.FLAG_READ

	def __init__(self):
		Characteristic.__init__(self, self.UUID, self.FLAGS)
