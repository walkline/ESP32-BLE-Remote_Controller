"""
The MIT License (MIT)
Copyright © 2020 Walkline Wang (https://walkline.wang)
Gitee: https://gitee.com/walkline/ESP32-BLE-Remote_Controller
"""
import ubluetooth as bt
from ble.const import BLEConst
from .__characteristic import Characteristic

class Report(Characteristic):
	UUID = bt.UUID(BLEConst.Characteristics.REPORT)
	FLAGS = bt.FLAG_READ | bt.FLAG_WRITE | bt.FLAG_NOTIFY

	def __init__(self):
		Characteristic.__init__(self, self.UUID, self.FLAGS)
