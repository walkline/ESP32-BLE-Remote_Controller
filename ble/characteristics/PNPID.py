"""
The MIT License (MIT)
Copyright © 2020 Walkline Wang (https://walkline.wang)
Gitee: https://gitee.com/walkline/ESP32-BLE-Remote_Controller
"""
import ubluetooth as bt
from ble.const import BLEConst
from .__characteristic import Characteristic

class PNPID(Characteristic):
	UUID = bt.UUID(BLEConst.Characteristics.PNP_ID)
	FLAGS = bt.FLAG_READ

	def __init__(self):
		Characteristic.__init__(self, self.UUID, self.FLAGS)
