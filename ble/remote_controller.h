char ReportDescriptor[21] = {
    0x05, 0x0c,                    // USAGE_PAGE (Consumer Devices)
    0x09, 0x01,                    // USAGE (Consumer Control)
    0xa1, 0x01,                    // COLLECTION (Application)
    0x05, 0x0c,                    //   USAGE_PAGE (Consumer Devices)
    0x09, 0xea,                    //   USAGE (Volume Down)
    0x15, 0x01,                    //   LOGICAL_MINIMUM (1)
    0x25, 0x01,                    //   LOGICAL_MAXIMUM (1)
    0x75, 0x01,                    //   REPORT_SIZE (1)
    0x95, 0x01,                    //   REPORT_COUNT (1)
    0x81, 0x06,                    //   INPUT (Data,Var,Rel)
    0xc0                           // END_COLLECTION
};
