"""
The MIT License (MIT)
Copyright © 2020 Walkline Wang (https://walkline.wang)
Gitee: https://gitee.com/walkline/ESP32-BLE-Remote_Controller
"""
import ubluetooth as bt
from ble.const import BLEConst
from .__service import Service

class GenericAttribute(Service):
	UUID = bt.UUID(BLEConst.Services.GENERIC_ATTRIBUTE)

	def __init__(self):
		Service.__init__(self, self.UUID)
