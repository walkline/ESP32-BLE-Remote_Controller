"""
The MIT License (MIT)
Copyright © 2020 Walkline Wang (https://walkline.wang)
Gitee: https://gitee.com/walkline/ESP32-BLE-Remote_Controller
"""
import ubluetooth as bt
from ble.const import BLEConst
from .__descriptor import Descriptor

class ReportReference(Descriptor):
	UUID = bt.UUID(BLEConst.Descriptors.REPORT_REFERENCE)
	FLAGS = bt.FLAG_READ

	def __init__(self):
		Descriptor.__init__(self, self.UUID, self.FLAGS)
