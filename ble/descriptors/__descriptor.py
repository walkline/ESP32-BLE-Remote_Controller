"""
The MIT License (MIT)
Copyright © 2020 Walkline Wang (https://walkline.wang)
Gitee: https://gitee.com/walkline/ESP32-BLE-Remote_Controller
"""
class Descriptor(object):
	def __init__(self, uuid, flags):
		self.__uuid = uuid
		self.__flags = flags

	def get_descriptor(self):
		return self.__uuid, self.__flags
